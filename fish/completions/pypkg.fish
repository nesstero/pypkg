set -l progname pypkg
set -l listall "(__fish_print_xbps_packages)"

complete -c $progname -f
complete -c $progname -a "$listall"
