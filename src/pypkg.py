#! /usr/bin/python
from subprocess import run, call, check_output, DEVNULL, STDOUT
from sys import argv

banner = """
 ______   ______  _  ______ 
|  _ \ \ / /  _ \| |/ / ___|
| |_) \ V /| |_) | ' / |  _ 
|  __/ | | |  __/| . \ |_| |
|_|    |_| |_|   |_|\_\____| v.0.3

Simple info package
$ pypkg <name_package>\t\t-- For Basic Info
$ pypkg -f <name_package>\t-- For Full Info
"""

class package:
    
    def __init__(self, namePkg):
        self.namePkg = namePkg
        cekNamePkg = call(["xbps-query", "-R", self.namePkg], stdout=DEVNULL, stderr=STDOUT)
        cekStatePkg = call(["xbps-query", "-S", self.namePkg], stdout=DEVNULL, stderr=STDOUT)
        if cekNamePkg == 2:
            print("Package not found, try to check again")
            exit()
        elif cekNamePkg == 0 and cekStatePkg == 0:
            self.desPkg = check_output(["xbps-query", "-S", self.namePkg], text=True).strip().split("\n")
            self.state = self.basicPart("state")
            self.state = self.state.replace("installed", "☑️")
            self.dateInstall = self.basicPart("install-date")
        else:
            self.desPkg = check_output(["xbps-query", "-R", self.namePkg], text=True).strip().split("\n")
            self.state = "❎"
            self.dateInstall = "-"

    def state(self):
        return self.state, self.dateInstall

    def basicPart(self, namePart):
        subPart = filter(lambda Part:namePart in Part, self.desPkg)
        subPart = list(subPart)
        Part = subPart[0].split(": ")
        Part = Part[1]
        return Part

    def part(self, namePart):
        subPart = filter(lambda Part:namePart in Part, self.desPkg)
        subPart = list(subPart)
        return subPart

    def runDepen(self):
        depenList = []
        rundepen = self.part("run_depends")
        shlib = self.part("shlib-provides")
        if len(shlib) <= 0:
            shlib = self.part("shlib-requires")
            if len(shlib) <=0:
                shlib = self.part("short_desc")
        if len(rundepen) <= 0:
            print(f"{self.namePkg} doesn't need any dependencies")
        else:
            indexRunDepen = self.desPkg.index(rundepen[0])
            indexShlib = self.desPkg.index(shlib[0])
            for i in range(indexRunDepen + 1, indexShlib):
                listDepen = self.desPkg[i]
                listDepen = listDepen.replace("\t", "")
                depenList.append(listDepen)
        for i, key in enumerate(depenList):
            if (i + 1) % 2:
                print(' 📦','{:30}'.format(key), end='\t')
            else:
                print(' 📦',key, end='\n')

    def shlibProv(self):
        shlibList = []
        shlibprov = self.part("shlib-provides")
        shlibreq = self.part("shlib-requires")
        if len(shlibprov) <= 0:
            print(f"{self.namePkg} has no shared library provide")
        else:
            indexShlibProv = self.desPkg.index(shlibprov[0])
            indexShlibReq = self.desPkg.index(shlibreq[0])
            for i in range(indexShlibProv + 1, indexShlibReq):
                listShlib = self.desPkg[i]
                listShlib = listShlib.replace("\t", "")
                shlibList.append(listShlib)
        for i, key in enumerate(shlibList):
            if (i + 1) % 2:
                print(' 🔗','{:30}'.format(key), end='\t')
            else:
                print(' 🔗',key, end='\n')

    def shlibReq(self):
        shlibList = [] 
        shlibreq = self.part("shlib-requires")
        shortdesc = self.part("short_desc")
        if len(shlibreq) <= 0:
            print(f"{self.namePkg} doesn't need any shared library")
        else:
            indexShlibReq = self.desPkg.index(shlibreq[0])
            indexShorDes = self.desPkg.index(shortdesc[0])
            for i in range(indexShlibReq + 1, indexShorDes ):
                    listShlib = self.desPkg[i]
                    listShlib = listShlib.replace("\t", "")
                    shlibList.append(listShlib)
            for i, key in enumerate(shlibList):
                if (i + 1) % 2:
                    print(' 🔗','{:30}'.format(key), end='\t')
                else:
                    print(' 🔗',key, end='\n')

    def sparator(self, spart, jml): 
        # lenInfo = [self.basicPart("homepage"), self.basicPart("short_desc"), self.basicPart("license")]
        # n = []
        # for x in lenInfo:
        #     y = len(x)
        #     n.append(y)
        # z = None
        # for x in n:
        #     if(z is None or x > z):
        #         z = x
        #         z = z + 7
        # sparator = spart * z
        # return sparator
        print(spart * jml)

    def installPkg(self):
        if self.dateInstall == "-":
            installOpt = input(f"\n📦 Install {self.namePkg} ? (Yes/no) : ")
            if installOpt == "Yes" or installOpt == "yes" or installOpt == "y" or installOpt == "Y" or installOpt == "":
                run(["sudo", "xbps-install", "-S", self.namePkg])
            else:
                exit()

    def baseInfo(self):
        print("")
        print(" 🌐 :", self.basicPart("homepage"))
        print(" 📑 :", self.basicPart("short_desc"))
        print(" 📦 :", self.basicPart("filename-size"))
        print(" 🔖 :", self.basicPart("pkgver"))
        print(" 🏷️ :", self.basicPart("license"))
        if self.dateInstall == "-":
            print(" 📥 :", self.state)
        else:
            print(" 📥 :", self.state, "/", self.dateInstall)
        print("")

    def pypkgBase(self):
        self.baseInfo()
        self.installPkg()

    def pypkgFull(self):
        self.baseInfo()
        self.sparator("=", 20)
        print("\033[1m ❄️ Dependencies :\033[0m")
        self.sparator("=", 20)
        self.runDepen()
        print("\n")
        self.sparator("=", 30)
        print("\033[1m ❄️ Shared Library Requires :\033[0m")
        self.sparator("=", 30)
        self.shlibReq()
        print("\n")
        self.sparator("=", 30)
        print("\033[1m ❄️ Shared Library Provides :\033[0m")
        self.sparator("=", 30)
        self.shlibProv()
        self.installPkg()

if len(argv) == 1:
    print(banner)
    exit()
elif argv[1] == "-f" and len(argv) <= 3:
    pypkg = package(argv[2])
    pypkg = pypkg.pypkgFull()
elif len(argv) != 2:
    print(banner)
    exit()
else:
    pypkg = package(argv[1])
    pypkg = pypkg.pypkgBase()
