# PyPkg
Simple info package

# Install
```
$ git clone https://gitlab.com/nesstero/pypkg.git
$ cd pypkg
$ cp bin/pypkg ~/.local/bin/
```

# Usage
```
$ pypkg
 ______   ______  _  ______ 
|  _ \ \ / /  _ \| |/ / ___|
| |_) \ V /| |_) | ' / |  _ 
|  __/ | | |  __/| . \ |_| |
|_|    |_| |_|   |_|\_\____| v.0.3

Simple info package
$ pypkg <name_package>          -- For Basic Info
$ pypkg -f <name_package>       -- For Full Info

```

# Screenshot
![installed](ss/installed.png)
![not_installed](ss/not_installed.png)
